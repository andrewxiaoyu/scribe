/**
 * Fields
 */
const lineNumbers = $('#notepad-line-numbers');
const lineSyllables = $('#notepad-syllables');
const notepad = $('#notepad');
const uploadInput = $("#upload");
const wordHeader = $("#word-header");
const words = $("#words");

/**
 * Pixel Measurements
 */
const letterHeight = 17;
const letterWidth = 9.687; //unused for now

/** Global Vars */
var row = 0;
var syllableNumbers = [];
var highlight = [
    {
        /**
         * Highlights current row
         */
        highlight: [0, 0],
        className: "currentline"
    },
    {
        /**
         * Highlight verse markers
         */
        highlight: /\[.*\]/g,
        className: "verse"
    },
];

/**
 * Setup
 */
$(document).ready(()=>{
    highlightText();

    uploadInput.change(uploadFile);
    notepad.focus();

    setupKeybinds();
});

/** Automatically resize */
$(document).on('input', 'textarea', function() {
    resizeNotepad();
});

function resizeNotepad(){
    notepad.outerHeight(38).outerHeight(notepad.prop('scrollHeight'));
}

/**
 * Uploading and Downloading
 */
function uploadFile(){
    if (!window.FileReader) {
        alert('Your browser does not support file uploads.');
    }
    let input = uploadInput.get(0);

    if (input.files.length) {
        let textFile = input.files[0];

        let reader = new FileReader();
        reader.readAsText(textFile);

        $(reader).on('load', (e)=>{
            var file = e.target.result,results;

            if (file && file.length) {
                notepad.val(file);
                resizeNotepad();
                updateNumbers();
                notepad.highlightWithinTextarea('update');
            }
        });
    } else {
        alert('Please upload a file.')
    }
}
function downloadFile(){
    let filename = prompt("Please enter a filename.");
    download(notepad.val(), filename, "text/plain");
}
 
/**
 * Pointer Functions
 */
function findPointer(){
    let pos = notepad.prop("selectionStart");
    row = notepad.val().substr(0, pos).split("\n").length - 1;
    highlightRow();
}
function highlightRow(){
    // Highlight Line
    let text = parseText();
    if(text.length > row){
        let startIndex = 0;
        _.each(text.slice(0,row), (t)=>startIndex += t.length);
        let endIndex = startIndex + text[row].length;
        highlight[0]["highlight"] = [startIndex, endIndex];
        notepad.highlightWithinTextarea('update');
    }

    // Line Numbers & Syllables
    let lines = lineNumbers.children();
    let syllables = lineSyllables.children();   
    _.each(lines, (c)=>{
        $(c).removeClass('selected');
    });
    _.each(syllables, (c)=>{
        $(c).removeClass('selected');
    });
    $(lines[row]).addClass('selected');
    $(syllables[row]).addClass('selected');
}

/**
 * Updating Numbers
 * 
 * Used to update line numbers and syllable count for each line.
 */
function updateNumbers(){
    let text = parseText();
    setupLineNumbers(text);
    findPointer();
    setupSyllableNumbers(text);
    parseVerses();
}
function parseText(){
    let text = notepad.val();
    text = text.split(/$/gm);
    return text;
}
function setupLineNumbers(text){
    lineNumbers.empty();
    notepadContentCount = Math.max(1, text.length);
    for(let l = 1; l<=notepadContentCount; l++){
        lineNumbers.append("<div>"+l+"</div>");
    }
}
function setupSyllableNumbers(text){
    $.post("/syllables",{
        text: JSON.stringify(text),
        syllables: JSON.stringify(syllableNumbers)
    }).done((res)=>{
        lineSyllables.empty();
        for(let l = 0; l<res.length; l++){
            let v = parseInt(res[l].count) > 0 ? res[l].count : 0;
            lineSyllables.append("<div>"+v+"</div>");
        }
        highlightRow();
        syllables = res;
    })
}

/**
 * Text Highlighting
 */
function highlightText(){
    notepad.highlightWithinTextarea({
        highlight: highlight
    });
}

/**
 * Verses
 */
function parseVerses(){
    let text = notepad.val();
    text = text.split(/\[/g);
    text = text.slice(1,);
    let verses = _.map(text, (t)=>{
        let verse = t.split(/\]/gm);
        let header = verse[0];
        let content = verse.slice(1,);
        return {header: content};
    });
}

/**
 * Info Box
 */
function setupKeybinds(){
    notepad.keypress( (e)=> {
        if ( e.ctrlKey || e.metaKey ){
            if ( e.which == 98 ) { // Ctrl B
                // Find Rhyme
                e.preventDefault();
                getRhymes();
            }
            if ( e.which == 105 ) { // Ctrl I
                // Find Stresses
                e.preventDefault();
                getStresses();
            }
            
        }
        if ( e.which == 126 ) { // ~
            // Piece Stats
            e.preventDefault();
            getOverview();
        }
    });
}
function getRhymes(){
    let start = notepad.prop("selectionStart");
    let end = notepad.prop("selectionEnd");
    let query = notepad.val().substring(start, end);

    words.empty();
    wordHeader.text("Rhymes");
    if(query){
        $.post("/rhymes",{
            query: JSON.stringify(query)
        }).done((res)=>{
            
            if(res.length > 0){
                for(let i = 0; i<res.length; i++){
                    words.append("<li>" + res[i] + "</li>");
                }
            }
        }).fail((res)=>{
            alert(res.statusText);
        })
    }
}
function getStresses(){
    let start = notepad.prop("selectionStart");
    let end = notepad.prop("selectionEnd");
    let query = notepad.val().substring(start, end);

    words.empty();
    wordHeader.text("Stresses");
    if(query){
        $.post("/stresses",{
            query: JSON.stringify(query)
        }).done((res)=>{
            if(res.length > 0){
                for(let i = 0; i<res.length; i++){
                    words.append("<li>" + res[i] + "</li>");
                }
            }
        }).fail((res)=>{
            alert(res.statusText);
        })
    }
}
function getOverview(){
    let query = notepad.val();
    if(query){
        $.post("/overview",{
            text: JSON.stringify(query)
        }).done((res)=>{
            words.empty();
            wordHeader.text("Overview");
            for (const [key, value] of Object.entries(res)) {
                words.append("<li>" + key + ": " + value + "</li>");
            }
        }).fail((res)=>{
            alert(res.statusText);
        })
    }
}