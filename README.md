# Scribe Lyric Writer

Scribe is a basic text editor for songwriters and poets. It provides a syllable counter, a simple rhyming dictionary, a stress detector, and text analytics.

Click here to try it out: [Scribe](https://scribe-writer.herokuapp.com/)

## Keybinds
| Keybind       | Feature       | 
| ------------- |---------------| 
| **Ctrl+B**       | Produces a list of rhymes (one word only)       | 
| **Ctrl+I**       | Show stresses for selection       | 
| **~**       | Text analysis       | 

## Thanks to
* [textacy](https://chartbeat-labs.github.io/textacy/)
* [pronouncing](https://pypi.org/project/pronouncing/)
* [download.js](http://danml.com/download.html)
* [highlight-within-textarea](https://github.com/lonekorean/highlight-within-textarea)
* [underscore.js](http://underscorejs.org/)