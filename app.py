from flask import Flask, render_template, jsonify, request
from pprint import pprint
import re
import textacy
import spacy
import en_core_web_sm
import json
import pronouncing

app = Flask(__name__)
nlp = en_core_web_sm.load()

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/about')
def about():
    return render_template('info.html')

@app.route('/syllables', methods=['POST'])
def parse_request():
    data = request.form.get('text', '')
    past_syllables = request.form.get('syllables', '')
    data = json.loads(data)
    past_syllables = json.loads(past_syllables)
    data = [strip_html(d).rstrip().lstrip() for d in data]

    syllables = []
    for i in range(len(data)):
        syllable = None
        for t in range(len(past_syllables)):
            if data[i] == past_syllables[t].text:
                syllable = {
                    'text': data[i],
                    'count': past_syllables[t].count
                }
        if not syllable:
            syllable = {
                'text': data[i],
                'count': count_syllables(data[i])
            }
        syllables.append(syllable)
    return jsonify(syllables)

def strip_html(data):
    p = re.compile(r'<.*?>')
    return p.sub('', data)

def count_syllables(data):
    doc = nlp(data)
    ts = textacy.TextStats(doc)
    syllables = ts.basic_counts['n_syllables']
    syllables -= data.count("\'")
    return syllables
    # syllables = 0
    # for w in data.split(' '):
    #     pronunciation_list = pronouncing.phones_for_word("programming")
    #     syllables += pronouncing.syllable_count(pronunciation_list[0])
    # return syllables

@app.route('/overview', methods=['POST'])
def text_data():
    data = request.form.get('text', '')
    data = json.loads(data)

    doc = nlp(data)
    ts = textacy.TextStats(doc)

    payload = {}
    payload['Readability Index'] = ts.readability_stats['automated_readability_index']
    payload['Flesch Kincaid Grade Level'] = ts.readability_stats['automated_readability_index']
    payload['Total Words'] = ts.basic_counts['n_words']
    payload['Unique Words'] = ts.basic_counts['n_unique_words']
    payload['Long Words'] = ts.basic_counts['n_long_words']
    payload['Monosyllable Words'] = ts.basic_counts['n_monosyllable_words']
    payload['Polysyllable Words'] = ts.basic_counts['n_polysyllable_words']
    return jsonify(payload)

@app.route('/rhymes', methods=['POST'])
def rhymes():
    data = request.form.get('query', '')
    data = json.loads(data).rstrip().lstrip()
    rhymes = pronouncing.rhymes(data)
    return jsonify(rhymes)

@app.route('/stresses', methods=['POST'])
def stresses():
    data = request.form.get('query', '')
    data = json.loads(data).rstrip().lstrip()

    stresses = []
    for word in data.split(' '):
        phones_list = pronouncing.phones_for_word(word)
        if phones_list:
            stress_pattern = pronouncing.stresses(phones_list[0])

            pattern = ""
            for l in stress_pattern:
                if l == '1':
                    pattern += 'S'
                if l == '2':
                    pattern += 's'
                if l == '0':
                    pattern += '_'

            stresses.append(pattern)
        stresses.append('n/a')
        
    return jsonify(stresses)

if __name__ == '__main__':
    app.run()